import Menu from './Menu.vue'

export default {
  title: 'Menu',
  component: Menu,
}

const Template = (args) => ({
  components: { Menu },
  setup() {
    return { args }
  },

  template: '<Menu v-bind="args" />',
})

export const Default = Template.bind({})
Default.args = {
  links: [
    { id: 1, text: 'County Line' },
    { id: 2, text: 'Strands' }
  ],
}

export const Empty = Template.bind({})
Empty.args = {
  links: [],
}