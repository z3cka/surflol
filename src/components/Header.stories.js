import Header from './Header.vue'
import * as MenuStories from './Menu.stories'

export default {
  title: 'Header',
  component: Header,
}

const Template = (args) => ({
  components: { Header },
  setup() {
    return { args }
  },
  template: '<Header v-bind="args" />',
})

export const Default = Template.bind({})
Default.args = {
  spotTitle: 'County Line',
  links: [
    ...MenuStories.Default.args.links,
  ],
}

export const Empty = Template.bind({})
Empty.args = {
  spotTitle: 'County Line',
  links: [],
}
